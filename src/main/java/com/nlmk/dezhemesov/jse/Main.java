package com.nlmk.dezhemesov.jse;

import com.nlmk.dezhemesov.jse.service.MathService;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Основной класс и обработчик консольных команд
 */
public class Main {

    private static final String FEW_ARGUMENTS = "Too few arguments";
    private static final String ILLEGAL_ARGUMENTS = "Some arguments are illegal";

    private MathService mathService = new MathService();

    public boolean help(String[] args) {
        System.out.println("Commands supported:");
        System.out.println("sum arg1 arg2");
        System.out.println("factorial arg");
        System.out.println("fibonacci arg");
        System.out.println("help");
        System.out.println("quit");
        System.out.println();
        return true;
    }

    public boolean quit(String[] args) {
        System.out.println("Good bye!");
        System.out.println();
        return false;
    }

    private void error(String errorText) {
        System.out.println("ERROR: " + errorText);
    }

    public boolean sum(String[] args) {
        if (args.length < 3) {
            error(FEW_ARGUMENTS);
            return true;
        }
        try {
            long result = mathService.sum(args[1], args[2]);
            System.out.println("Sum of arguments: " + result);
        } catch (IllegalArgumentException e) {
            error(ILLEGAL_ARGUMENTS);
        }
        System.out.println();
        return true;
    }

    public boolean factorial(String[] args) {
        if (args.length < 2) {
            error(FEW_ARGUMENTS);
            return true;
        }
        try {
            long result = mathService.factorial(args[1]);
            System.out.println("Factorial of argument: " + result);
        } catch (IllegalArgumentException e) {
            error(ILLEGAL_ARGUMENTS);
        }
        System.out.println();
        return true;
    }

    public boolean fibonacci(String[] args) {
        if (args.length < 2) {
            error(FEW_ARGUMENTS);
            return true;
        }
        try {
            long[] result = mathService.fibonacci(args[1]);
            System.out.print("Fibonacci representation of argument: " + Arrays.toString(result));
        } catch (IllegalArgumentException e) {
            error(ILLEGAL_ARGUMENTS);
        }
        System.out.println();
        return true;
    }

    public void consoleLoop() {
        Scanner scanner = new Scanner(System.in);
        help(null);
        while (true) {
            System.out.print(": ");
            String command = scanner.nextLine();
            String[] args = command.split(" ");
            Method method = null;
            try {
                method = this.getClass().getMethod(args[0], String[].class);
            } catch (NoSuchMethodException e) {
                System.out.println("Command " + args[0] + " is not supported!");
                help(null);
            }
            boolean b = true;
            if (method != null) {
                try {
                    b = (boolean) method.invoke(this, (Object) args);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if (!b) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        Main proc = new Main();
        proc.consoleLoop();
    }
}
