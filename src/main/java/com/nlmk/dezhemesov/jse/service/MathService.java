package com.nlmk.dezhemesov.jse.service;

import java.util.ArrayList;

/**
 * Сервис для математических вычислений
 */
public class MathService {

    public long sum(String arg1, String arg2) {
        try {
            Long longArg1 = Long.parseLong(arg1);
            Long longArg2 = Long.parseLong(arg2);
            return longArg1 + longArg2;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    public long factorial(String arg) {
        Long longArg;
        try {
            longArg = Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
        if (longArg < 0) {
            throw new IllegalArgumentException();
        }
        long result = 1L;
        for (long i = 1L; i <= longArg; i++) {
            try {
                result = Math.multiplyExact(result, i);
            } catch (ArithmeticException e) {
                throw new IllegalArgumentException();
            }
        }
        return result;
    }

    private long computeLargestFibbLessThan(long n) {
        if (n == 0 || n == 1)
            return n;
        long fib1;
        long fib2 = 1L;
        long fib3 = 1L;
        while (fib3 <= n) {
            fib1 = fib2;
            fib2 = fib3;
            fib3 = fib1 + fib2;
        }
        return fib2;
    }

    public long[] fibonacci(String arg) {
        Long longArg;
        try {
            longArg = Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
        if (longArg < 0) {
            throw new IllegalArgumentException();
        }
        ArrayList<Long> fibbList = new ArrayList<>();
        while (longArg > 0) {
            long fib = computeLargestFibbLessThan(longArg);
            fibbList.add(fib);
            longArg -= fib;
        }
        return fibbList.stream().mapToLong(l -> l).toArray();
    }

}
