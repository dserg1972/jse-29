package com.nlmk.dezhemesov.jse.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathServiceTest {

    MathService mathService;

    @BeforeEach
    void setup() {
        mathService = new MathService();
    }

    @Test
    void sum() {
        assertEquals(7, mathService.sum("4", "3"));
        assertEquals(-7, mathService.sum("-4", "-3"));
        assertThrows(IllegalArgumentException.class, () -> mathService.sum("12.34", "77"));
        assertThrows(IllegalArgumentException.class, () -> mathService.sum("12", "77.78"));
    }

    @Test
    void factorial() {
        assertEquals(20922789888000L, mathService.factorial("16"));
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("-2"));
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("12.345"));
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("1024"));
    }

    @Test
    void fibonacci() {
        long[] fibbs = {21L, 8L, 1L};
        assertArrayEquals(fibbs, mathService.fibonacci("30"));
        assertThrows(IllegalArgumentException.class, ()->mathService.fibonacci("-10"));
        assertThrows(IllegalArgumentException.class, ()->mathService.fibonacci("123.456"));
    }
}